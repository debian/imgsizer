<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC 
   "-//OASIS//DTD DocBook XML V4.1.2//EN"
   "docbook/docbookx.dtd">
<refentry id='imgsizer.man'>
<refmeta>
<refentrytitle>imgsizer</refentrytitle>
<manvolnum>1</manvolnum>
<refmiscinfo class='date'>6 Sep 2001</refmiscinfo>
<refmiscinfo class='productname'>imgsizer</refmiscinfo>
<refmiscinfo class='source'>imgsizer</refmiscinfo>
<refmiscinfo class='manual'>Web Tools</refmiscinfo>
</refmeta>
<refnamediv id='name'>
<refname>imgsizer</refname>
<refpurpose>automatically splice in height and width params for HTML IMG tags</refpurpose>
</refnamediv>
<refsynopsisdiv id='synopsis'>

<cmdsynopsis>
  <command>imgsizer</command>  
      <arg choice='opt'><option>-d</option> <replaceable>file</replaceable></arg>
      <arg choice='opt'>--document-root <replaceable>file</replaceable></arg>
      <arg choice='opt'><option>-h</option> <replaceable>file</replaceable></arg>
      <arg choice='opt'>--help <replaceable>file</replaceable></arg>
      <arg choice='opt'><option>-n</option></arg>
      <arg choice='opt'><option>--no-overwrite</option></arg>
      <arg choice='opt'><replaceable>HTMLFile</replaceable></arg>
      <arg choice='opt'><option>-v</option> <replaceable>file</replaceable></arg>
      <arg choice='opt'>--version</arg>
</cmdsynopsis>

</refsynopsisdiv>

<refsect1 id='options'><title>OPTIONS</title>
<variablelist>
<varlistentry>
<term>
<cmdsynopsis>
  <arg choice='plain'>-V, </arg>
  <arg choice='plain'>--version </arg>
</cmdsynopsis>
</term>
<listitem>
<para>Display version information and exit.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
<cmdsynopsis>
  <arg choice='plain'>-h, </arg>
  <arg choice='plain'>--help </arg>
</cmdsynopsis>
</term>
<listitem>
<para>Display usage information.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
<cmdsynopsis>
  <arg choice='plain'>-d <replaceable>&lt;directory&gt;,</replaceable></arg>
  <arg choice='plain'>--document-root <replaceable>&lt;directory&gt;</replaceable></arg>
</cmdsynopsis>
</term>
<listitem>
<para>Directory where absolute image filenames (i.e, ones which contain
a leading "/") may be found.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis role='bold'>-n, --no-overwwrite</emphasis></term>
<listitem><para>Don't overwrite existing width and height tags if both are present.</para></listitem>
</varlistentry>
</variablelist>
</refsect1>


<refsect1 id='description'><title>DESCRIPTION</title>

<para>The <command>imgsizer</command> script automates away the
tedious task of creating and updating the extension HEIGHT and WIDTH
parameters in HTML IMG tags.  These parameters help many browsers
(including the Netscape/Mozilla family) to multi-thread image loading,
instead of having to load images in strict sequence in order to have
each one's dimensions available so the next can be placed.  This
generally allows text on the remainder of the page to load much
faster.</para>

<para>This script will try create such attributes for any IMG tag that lacks
them.  It will correct existing HEIGHT and WIDTH tags unless either
contains a percent (%) sign, in which case the existing dimensions
are presumed to be relative and left unaltered.</para>

<para>This script may be called with no arguments.  In this mode, it filters
HTML presented on stdin to HTML (unaltered except for added or
corrected HEIGHT and WIDTH attributes) on stdout.  If called with file
arguments, it will attempt to transform each file in place.  Each
argument file is not actually modified until the script completes a
successful conversion pass.</para>

<para>The <option>-d</option> &lt;directory&gt; option sets the
DocumentRoot, where images with an absolute filename (i.e., ones which
contain a leading "/") may be found.  If none is specified, the
DocumentRoot defaults to the current working directory.</para>

<para>The -n (no-overwrite) opion prevents the program from overwriting 
existing width and height tags if both are present.</para>

<para>Additional options may also be specified in the environmental
variable "IMGSIZER".  For example, to avoid typing "imgsizer
<option>-d</option> /var/www/docs" each time
<command>imgsizer</command> is invoked, you might tell sh (or one of
its descendants):</para>

<programlisting>
IMGSIZER="-d /var/www/docs"; export IMGSIZER
</programlisting>

<para>or, if you use csh:</para>

<programlisting>
setenv IMGSIZER "-d /var/www/docs"
</programlisting>

<para>This script is written in Python, and thus requires a Python
interpreter on the host system.  It also requires either the
<citerefentry><refentrytitle>identify</refentrytitle><manvolnum>1</manvolnum></citerefentry>
utility distributed in the open-source ImageMagick suite of
image-display and manipulation tools, or a modern version of
<citerefentry><refentrytitle>file</refentrytitle><manvolnum>1</manvolnum></citerefentry>
and
<citerefentry><refentrytitle>rdjpgcom</refentrytitle><manvolnum>1</manvolnum></citerefentry>.
These utilities are used to extract sizes from the images;
<command>imgsizer</command> itself has no knowledge of graphics
formats.  The script will handle any image format known to
<citerefentry><refentrytitle>identify</refentrytitle><manvolnum>1</manvolnum></citerefentry>
including PNG, GIF, JPEG, XBM, XPM, PostScript, BMP, TIFF, and
anything else even remotely likely to show up as an inline
image.</para>

</refsect1>

<refsect1 id='bugs'><title>NOTE</title>
<para>The -q, -l, and -m options of the 1.0 versions are gone.  What they
used to do has been made unnecessary by smarter logic.</para>
</refsect1>

<refsect1 id='note'><title>BUGS</title>
<para>The code uses regular expressions rather than true HTML/XML parsing. 
Some perverse but legal constructions, like extraneous space within quoted
numeric attributes, will be mangled.</para>
</refsect1>

<refsect1 id='author'><title>AUTHOR</title>
<para>Originally created by Eric S. Raymond &lt;esr@thyrsus.com&gt;.
Additional code contributed by Erik Rossen, Michael C. Toren
&lt;michael@toren.net&gt;, and others.  For updates, see &lt;<ulink
url='http://www.catb.org/~esr'>http://www.catb.org/~esr</ulink>&gt;</para>
</refsect1>

<refsect1 id='see_also'><title>SEE ALSO</title>
<para><citerefentry><refentrytitle>identify</refentrytitle><manvolnum>1</manvolnum></citerefentry>,
<citerefentry><refentrytitle>file</refentrytitle><manvolnum>1</manvolnum></citerefentry>,
<emphasis remap='I'>rdjpgcom(1).</emphasis></para>
</refsect1>
</refentry>

